import React from "react";
import { Link, useNavigate, useParams } from "react-router-dom";

function LanguageComp(props) {
  const { name, image, viewbtn } = props;
  const { nameParams } = useParams();
  return (
    <div className="language-item">
      {!viewbtn && <h1>ID : {nameParams}</h1>}
      <div className="language-name">{name}</div>
      <img className="language-image" src={image} />
      {viewbtn && (
        <Link
          to={{
            pathname: name,
          }}
        >
          <button>view</button>
        </Link>
      )}
      {!viewbtn && (
        <Link
          to={{
            pathname: "/language",
          }}
        >
          <button>back</button>
        </Link>
      )}
    </div>
  );
}

export default LanguageComp;
