import logo from "./logo.svg";
import "./App.css";
import { Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Link to="/language">
          <a className="App-link" target="_blank" rel="noopener noreferrer">
            Move to language page
          </a>
        </Link>
      </header>
    </div>
  );
}

export default App;
