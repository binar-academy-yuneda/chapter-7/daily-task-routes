import "../App.css";
import LanguageComp from "../components/LanguageComponent";
import HeaderLanguage from "../components/Header";
import { languageList } from "../data/Data";

function LanguagePage() {
  // const { name } = useParams();
  return (
    <div className="App">
      <HeaderLanguage />
      {languageList.map((item) => {
        return (
          <LanguageComp
            key={item.name}
            name={item.name}
            image={item.image}
            viewbtn={true}
          />
        );
      })}
    </div>
  );
}

export default LanguagePage;
