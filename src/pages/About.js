import logo from "../logo.svg";
import "../App.css";
import { Link } from "react-router-dom";

function About() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Iki Halaman About</p>
        <Link to="/language">
          <a className="App-link" target="_blank" rel="noopener noreferrer">
            Move to language page
          </a>
        </Link>
      </header>
    </div>
  );
}

export default About;
