import "../App.css";
import LanguageComp from "../components/LanguageComponent";
import HeaderLanguage from "../components/Header";
import { languageList } from "../data/Data";
import { useParams } from "react-router-dom";

function DetailLanguage() {
  const { nameParams } = useParams();
  return (
    <div className="App">
      <HeaderLanguage />
      {languageList.map((item) => {
        if (item.name === nameParams) {
          return (
            <LanguageComp
              key={item.name}
              name={item.name}
              image={item.image}
              viewbtn={false}
            />
          );
        }
      })}
    </div>
  );
}

export default DetailLanguage;
